/**
 * Import function triggers from their respective submodules:
 *
 * const {onCall} = require("firebase-functions/v2/https");
 * const {onDocumentWritten} = require("firebase-functions/v2/firestore");
 *
 * See a full list of supported triggers at https://firebase.google.com/docs/functions
 */

const functions = require("firebase-functions");
const logger = require("firebase-functions/logger");
const axios = require('axios');
const admin = require("firebase-admin");

var serviceAccount = require("./token-history-firebase-adminsdk-azb9y-03d15c65c4.json");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://token-history-default-rtdb.firebaseio.com"
});

// Create and deploy your first functions
// https://firebase.google.com/docs/functions/get-started

const getLastTimestamp = async (project) => {
    const thisRef = admin.database().ref(`${project}/lastTimestamp`);
    return thisRef.once('value', (data) => {
        return data.val();
    });
};

const getFirstTimestamp = async (project) => {
    const thisRef = admin.database().ref(`${project}/firstTimestamp`);
    return thisRef.once('value', (data) => {
        return data.val();
    });
};

const getLastStop = async (project) => {
    const thisRef = admin.database().ref(`${project}/lastStop`);
    return thisRef.once('value', (data) => {
        return data.val();
    });
};

const getCurrentHolders = async (project) => {
    const thisRef = admin.database().ref(`${project}/holders`);
    return thisRef.once('value', (data) => {
        return data.val();
    });
};

const getTransfers = async (project, startTimestamp, endTimestamp) => {
    const thisRef = admin.database().ref(`${project}/transactions`);
    return thisRef.orderByChild('block_timestamp').startAt(startTimestamp).endAt(endTimestamp).once('value', (data) => {
        return data.val();
    });
};

const getTokenDecimals = async (project) => {
    const thisRef = admin.database().ref(`${project}/decimals`);
    return thisRef.once('value', (data) => {
        return data.val();
    });
};

const setNewTransactions = async (project, timestamp, updates, holders) => {
    const thisRef = admin.database().ref(`${project}/transactions`);
    const thisTimestampRef = admin.database().ref(`${project}/lastStop`);
    const thisHoldersRef = admin.database().ref(`${project}/holders`);
    await thisRef.update(updates);
    await thisHoldersRef.update(holders);
    await thisTimestampRef.set(timestamp);
};

exports.helloWorld = functions.https.onRequest((request, response) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

  logger.info("Hello logs!", {structuredData: true});
  response.send("Hello from Firebase!");
});

// exports.refreshTransfersAndHolders = functions.https.onRequest(async (request, response) => {
// exports.refreshTransfersAndHolders = functions.pubsub.schedule('*/3 * * * *').onRun(async (context) => {
//     let lastTimestamp = (await getLastStop('shrap')).val();
//     const firstTimestamp = (await getFirstTimestamp('shrap')).val();
//     if (lastTimestamp < firstTimestamp) {
//         console.error(lastTimestamp, firstTimestamp);
//         return;
//     }
//     // let page = 1;
//     let transfers = {};
//     let holders = (await getCurrentHolders('shrap')).val();
//     if (!holders) holders = {};
//     const tokenDecimals = (await getTokenDecimals('shrap')).val();
//     // console.log(tokenAddress, process.env.REACT_APP_CHAIN_ID);
//     while (true) {
//         try {
//             const options = {
//                 url: `https://api.chainbase.online/v1/token/transfers?chain_id=43114&contract_address=0xd402298a793948698b9a63311404FBBEe944eAfD&from_timestamp=${firstTimestamp}&end_timestamp=${lastTimestamp}&limit=100`,
//                 method: 'GET',
//                 headers: {
//                     'x-api-key': '2amM4p0H9B9PgpRr3SURTgUz3cY', // Replace the field with your API key.
//                     'accept': 'application/json'
//                 }
//             };
//             const r = await axios(options);
//             let transactions = r.data['data'];
//             let newLastTimestamp = 0;
//             for (let i = 0; i < transactions.length; i++) {
//                 // console.log(transactions.length);
//                 // console.log(transactions[i].block_timestamp);
//                 const fromAddress = transactions[i].from_address;
//                 const toAddress = transactions[i].to_address;
//                 const hash = transactions[i].transaction_hash;
//                 const realVal = parseFloat(transactions[i].value) / Math.pow(10, tokenDecimals);
//                 transfers[hash] = transactions[i];
//                 newLastTimestamp = Math.floor((new Date(transactions[i].block_timestamp)).getTime() / 1000.0);
//                 transactions[i].block_timestamp = newLastTimestamp;
//                 // console.error(newLastTimestamp);
//                 if (!holders[fromAddress]) holders[fromAddress] = 0;
//                 if (!holders[toAddress]) holders[toAddress] = 0;
//                 holders[fromAddress] -= realVal;
//                 holders[toAddress] += realVal;
//             }
//             lastTimestamp = Math.min(lastTimestamp, newLastTimestamp);
//             console.error(lastTimestamp);
//             console.error('------------------------------------------------------------');
//             await setNewTransactions('shrap', lastTimestamp, transfers, holders);
//             // page++;
//         } catch (err) {
//             logger.error(err);
//             logger.info('Finished');
//             break;
//         }
//     }

//     logger.info('Success');
//     // response.status(200).json({});
// });

exports.getTransferData = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    let start = request.query.start;
    let end = request.query.end;

    if (!start || !end) {
        response.status(400).json({ message: 'Missing start or end' });
        return;
    }

    start = parseInt(start);
    if (isNaN(start)) {
        response.status(400).json({ message: 'Start is not a number' });
        return;
    }

    end = parseInt(end);
    if (isNaN(end)) {
        response.status(400).json({ message: 'End is not a number' });
        return;
    }

    if (end < start) {
        response.status(400).json({ message: 'End must be greater than start' });
        return;
    }

    const holders = (await getCurrentHolders('shrap')).val();
    const transfers = (await getTransfers('shrap', start, end)).val();

    response.status(200).json({ message: 'Success', transfers: transfers, holders: holders });
});

exports.getFirstTimestamp = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    const timestamp = (await getFirstTimestamp('shrap')).val();
    response.status(200).json({ message: 'Success', timestamp: timestamp });
});

exports.getLastTimestamp = functions.https.onRequest(async (request, response) => {
    response.set('Access-Control-Allow-Origin', '*');
    response.set('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

    const timestamp = (await getLastTimestamp('shrap')).val();
    response.status(200).json({ message: 'Success', timestamp: timestamp });
});
